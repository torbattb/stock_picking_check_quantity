# -*- coding: utf-8 -*-
import time
import math
from odoo.osv import expression
from odoo.tools.float_utils import float_round as round
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.exceptions import UserError, ValidationError
from odoo import api, fields, models, _
from itertools import groupby
from odoo.tools import OrderedSet
from odoo.tools.float_utils import float_round, float_compare, float_is_zero

class stock_picking(models.Model):
    _inherit = 'stock.picking'
    @api.model
    def create(self, vals):
        defaults = self.default_get(['name', 'picking_type_id'])
        picking_type = self.env['stock.picking.type'].browse(vals.get('picking_type_id', defaults.get('picking_type_id')))
        picking_location = self.env['stock.location'].browse(vals.get('location_id'))   #START
        if picking_type.code in ('internal','outgoing'):
            mliwk = vals.get('move_line_ids_without_package')
            miwk = vals.get('move_ids_without_package')
            mlni = vals.get('move_line_nosuggest_ids')
            if mliwk:
                sum_quant_picking_draft_all = 0.0
                sum_quant_so_draft_all = 0.0
                for line1 in mliwk:
                    prod_name = self.env['product.product'].browse(line1[2]['product_id'])
                    requesting_quant = line1[2]['qty_done']
                    loc_ids = []
                    if picking_location:
                        loc_ids.append(picking_location.id)
                        for line in picking_location.child_ids:
                            loc_ids.append(line.id)
                    if len(loc_ids)==1:
                        quants_data = self.env['stock.quant'].search([('product_id','=',line1[2]['product_id']),('location_id','=',loc_ids[0])])
                    else:
                        quants_data = self.env['stock.quant'].search([('product_id','=',line1[2]['product_id']),('location_id','in',tuple(loc_ids))])
                    avail_quant_sum = 0.0
                    quantity = 0.0
                    reserved_quantity = 0.0
                    avail_quant = 0.0
                    if quants_data:
                        for quant_line in quants_data:
                            quantity += quant_line.quantity
                            reserved_quantity += quant_line.reserved_quantity
                        avail_quant = quantity - reserved_quantity
                        avail_quant_sum = avail_quant_sum + avail_quant
                    picking_types = self.env['stock.picking.type'].search([('code','in',('internal','outgoing')),('default_location_src_id','=',picking_location.id)])
                    list_type = []
                    for line0 in picking_types:
                        list_type.append(line0.id)
                    draft_picking_data = self.env['stock.picking'].search([('picking_type_id','in',list_type),('state','=','draft'),('location_id','=',picking_location.id),('id','!=',self.id)])
                    sum_quant_picking_draft_all = 0.0
                    sum_quant_picking_draft = 0.0
                    sum_quant_picking_draft_miwk = 0.0
                    sum_quant_picking_draft_mlni = 0.0
                    if draft_picking_data:
                        sum_quant_picking_draft = 0.0
                        for line2 in draft_picking_data:
                            onebyone_quant_picking_draft = 0.0
                            onebyone_quant_picking_draft_miwk = 0.0
                            onebyone_quant_picking_draft_mlni = 0.0
                            if line2.move_line_ids_without_package:
                                for line3 in line2.move_line_ids_without_package:
                                    if line3.product_id.id == line1[2]['product_id']:
                                        onebyone_quant_picking_draft = line3.qty_done
                            elif line2.move_ids_without_package:
                                for line32 in line2.move_ids_without_package:
                                    if line32.product_id.id == line1[2]['product_id']:
                                        onebyone_quant_picking_draft = line32.product_uom_qty
                            elif line2.move_line_nosuggest_ids:
                                for line33 in line2.move_line_nosuggest_ids:
                                    if line33.product_id.id == line1[2]['product_id']:
                                        onebyone_quant_picking_draft = line33.qty_done
                            sum_quant_picking_draft = sum_quant_picking_draft + onebyone_quant_picking_draft
                    sum_quant_picking_draft_all = sum_quant_picking_draft_all + sum_quant_picking_draft
                    bolomjit_too = avail_quant_sum - sum_quant_picking_draft_all
                    if bolomjit_too < requesting_quant:
                        raise ValidationError(_('''Do not order in large quantities from the POSSIBLE BALANCE at the location! 
                        Product name : %s, 
                        On hand Quantity : + %s,
                        Reserved Quantity : - %s,
                        Draft Pickings : - %s,
                        Available Quantity: = %s, 
                        Ordering Quantity : %s. 
                        You cannot order more than %s! Check the balance carefully!''')%(prod_name.name,quantity,reserved_quantity,sum_quant_picking_draft_all,bolomjit_too,line1[2]['qty_done'],bolomjit_too))
                    else:
                        sum_quant_picking_draft_all = 0.0
                        sum_quant_so_draft_all = 0.0
            elif miwk:
                sum_quant_picking_draft_all = 0.0
                sum_quant_so_draft_all = 0.0
                for line1 in miwk:
                    prod_name = self.env['product.product'].browse(line1[2]['product_id'])
                    requesting_quant = line1[2]['product_uom_qty']
                    loc_ids = []
                    if picking_location:
                        loc_ids.append(picking_location.id)
                        for line in picking_location.child_ids:
                            loc_ids.append(line.id)
                    if len(loc_ids)==1:
                        quants_data = self.env['stock.quant'].search([('product_id','=',line1[2]['product_id']),('location_id','=',loc_ids[0])])
                    else:
                        quants_data = self.env['stock.quant'].search([('product_id','=',line1[2]['product_id']),('location_id','in',tuple(loc_ids))])
                    avail_quant_sum = 0.0
                    quantity = 0.0
                    reserved_quantity = 0.0
                    avail_quant = 0.0
                    if quants_data:
                        for quant_line in quants_data:
                            quantity += quant_line.quantity
                            reserved_quantity += quant_line.reserved_quantity
                        avail_quant = quantity - reserved_quantity
                        avail_quant_sum = avail_quant_sum + avail_quant
                    picking_types = self.env['stock.picking.type'].search([('code','in',('internal','outgoing')),('default_location_src_id','=',picking_location.id)])
                    list_type = []
                    for line0 in picking_types:
                        list_type.append(line0.id)
                    draft_picking_data = self.env['stock.picking'].search([('picking_type_id','in',list_type),('state','=','draft'),('location_id','=',picking_location.id),('id','!=',self.id)])
                    sum_quant_picking_draft_all = 0.0
                    sum_quant_picking_draft = 0.0
                    sum_quant_picking_draft_miwk = 0.0
                    sum_quant_picking_draft_mlni = 0.0
                    if draft_picking_data:
                        sum_quant_picking_draft = 0.0
                        for line2 in draft_picking_data:
                            onebyone_quant_picking_draft = 0.0
                            onebyone_quant_picking_draft_miwk = 0.0
                            onebyone_quant_picking_draft_mlni = 0.0
                            if line2.move_line_ids_without_package:
                                for line3 in line2.move_line_ids_without_package:
                                    if line3.product_id.id == line1[2]['product_id']:
                                        onebyone_quant_picking_draft = line3.qty_done
                            elif line2.move_ids_without_package:
                                for line32 in line2.move_ids_without_package:
                                    if line32.product_id.id == line1[2]['product_id']:
                                        onebyone_quant_picking_draft = line32.product_uom_qty
                            elif line2.move_line_nosuggest_ids:
                                for line33 in line2.move_line_nosuggest_ids:
                                    if line33.product_id.id == line1[2]['product_id']:
                                        onebyone_quant_picking_draft = line33.qty_done
                            sum_quant_picking_draft = sum_quant_picking_draft + onebyone_quant_picking_draft
                    sum_quant_picking_draft_all = sum_quant_picking_draft_all + sum_quant_picking_draft
                    stock_warehouse = self.env['stock.warehouse'].search([('lot_stock_id','=',picking_location.id)])
                    bolomjit_too = avail_quant_sum - sum_quant_picking_draft_all
                    if bolomjit_too < requesting_quant:
                        raise ValidationError(_('''Do not order in large quantities from the POSSIBLE BALANCE at the location! 
                        Product name : %s, 
                        On hand Quantity : + %s,
                        Reserved Quantity : - %s,
                        Draft Pickings : - %s,
                        Available Quantity: = %s, 
                        Ordering Quantity : %s. 
                        You cannot order more than %s! Check the balance carefully!''')%(prod_name.name,quantity,reserved_quantity,sum_quant_picking_draft_all,bolomjit_too,line1[2]['product_uom_qty'],bolomjit_too))
                    else:
                        sum_quant_picking_draft_all = 0.0
                        sum_quant_so_draft_all = 0.0
            elif mlni:
                sum_quant_picking_draft_all = 0.0
                sum_quant_so_draft_all = 0.0
                for line1 in mlni:
                    prod_name = self.env['product.product'].browse(line1[2]['product_id'])
                    requesting_quant = line1[2]['qty_done']
                    loc_ids = []
                    if picking_location:
                        loc_ids.append(picking_location.id)
                        for line in picking_location.child_ids:
                            loc_ids.append(line.id)
                    if len(loc_ids)==1:
                        quants_data = self.env['stock.quant'].search([('product_id','=',line1[2]['product_id']),('location_id','=',loc_ids[0])])
                    else:
                        quants_data = self.env['stock.quant'].search([('product_id','=',line1[2]['product_id']),('location_id','in',tuple(loc_ids))])
                    avail_quant_sum = 0.0
                    quantity = 0.0
                    reserved_quantity = 0.0
                    avail_quant = 0.0
                    if quants_data:
                        for quant_line in quants_data:
                            quantity += quant_line.quantity
                            reserved_quantity += quant_line.reserved_quantity
                        avail_quant = quantity - reserved_quantity
                        avail_quant_sum = avail_quant_sum + avail_quant
                    picking_types = self.env['stock.picking.type'].search([('code','in',('internal','outgoing')),('default_location_src_id','=',picking_location.id)])
                    list_type = []
                    for line0 in picking_types:
                        list_type.append(line0.id)
                    draft_picking_data = self.env['stock.picking'].search([('picking_type_id','in',list_type),('state','=','draft'),('location_id','=',picking_location.id),('id','!=',self.id)])
                    sum_quant_picking_draft_all = 0.0
                    sum_quant_picking_draft = 0.0
                    sum_quant_picking_draft_miwk = 0.0
                    sum_quant_picking_draft_mlni = 0.0
                    if draft_picking_data:
                        sum_quant_picking_draft = 0.0
                        for line2 in draft_picking_data:
                            onebyone_quant_picking_draft = 0.0
                            onebyone_quant_picking_draft_miwk = 0.0
                            onebyone_quant_picking_draft_mlni = 0.0
                            if line2.move_line_ids_without_package:
                                for line3 in line2.move_line_ids_without_package:
                                    if line3.product_id.id == line1[2]['product_id']:
                                        onebyone_quant_picking_draft = line3.qty_done
                            elif line2.move_ids_without_package:
                                for line32 in line2.move_ids_without_package:
                                    if line32.product_id.id == line1[2]['product_id']:
                                        onebyone_quant_picking_draft = line32.product_uom_qty
                            elif line2.move_line_nosuggest_ids:
                                for line33 in line2.move_line_nosuggest_ids:
                                    if line33.product_id.id == line1[2]['product_id']:
                                        onebyone_quant_picking_draft = line33.qty_done
                            sum_quant_picking_draft = sum_quant_picking_draft + onebyone_quant_picking_draft
                    sum_quant_picking_draft_all = sum_quant_picking_draft_all + sum_quant_picking_draft
                    stock_warehouse = self.env['stock.warehouse'].search([('lot_stock_id','=',picking_location.id)])
                    
                    bolomjit_too = avail_quant_sum - sum_quant_picking_draft_all
                    if bolomjit_too < requesting_quant:
                        raise ValidationError(_('''Do not order in large quantities from the POSSIBLE BALANCE at the location! 
                        Product name : %s, 
                        On hand Quantity : + %s,
                        Reserved Quantity : - %s,
                        Draft Pickings : - %s,
                        Available Quantity: = %s, 
                        Ordering Quantity : %s. 
                        You cannot order more than %s! Check the balance carefully!''')%(prod_name.name,quantity,reserved_quantity,sum_quant_picking_draft_all,bolomjit_too,line1[2]['qty_done'],bolomjit_too))
                    else:
                        sum_quant_picking_draft_all = 0.0
                        sum_quant_so_draft_all = 0.0    #END
        if vals.get('name', '/') == '/' and defaults.get('name', '/') == '/' and vals.get('picking_type_id', defaults.get('picking_type_id')):
            if picking_type.sequence_id:
                vals['name'] = picking_type.sequence_id.next_by_id()
        moves = vals.get('move_lines', []) + vals.get('move_ids_without_package', [])
        if moves and vals.get('location_id') and vals.get('location_dest_id'):
            for move in moves:
                if len(move) == 3 and move[0] == 0:
                    move[2]['location_id'] = vals['location_id']
                    move[2]['location_dest_id'] = vals['location_dest_id']
                    picking_type = self.env['stock.picking.type'].browse(vals['picking_type_id'])
                    if 'picking_type_id' not in move[2] or move[2]['picking_type_id'] != picking_type.id:
                        move[2]['picking_type_id'] = picking_type.id
                        move[2]['company_id'] = picking_type.company_id.id
        scheduled_date = vals.pop('scheduled_date', False)
        res = super(stock_picking, self).create(vals)
        if scheduled_date:
            res.with_context(mail_notrack=True).write({'scheduled_date': scheduled_date})
        res._autoconfirm_picking()
        if vals.get('partner_id'):
            for picking in res.filtered(lambda p: p.location_id.usage == 'supplier' or p.location_dest_id.usage == 'customer'):
                picking.message_subscribe([vals.get('partner_id')])
        return res

class StockMove(models.Model):
    _inherit = "stock.move"
    def write(self, vals):
        receipt_moves_to_reassign = self.env['stock.move']
        if 'product_uom_qty' in vals:
            for move in self.filtered(lambda m: m.state not in ('done', 'draft') and m.picking_id):
                if float_compare(vals['product_uom_qty'], move.product_uom_qty, precision_rounding=move.product_uom.rounding):
                    self.env['stock.move.line']._log_message(move.picking_id, move, 'stock.track_move_template', vals)
            picking_type = self.picking_id.picking_type_id      #START
            picking_location = self.picking_id.location_id
            pr_id = False
            qty_id = 0.0
            if 'product_id' in vals and 'product_uom_qty' in vals:
                pr_id = vals['product_id']
                qty_id = vals['product_uom_qty']
            elif 'product_id' in vals and 'product_uom_qty' not in vals:
                pr_id = vals['product_id']
                qty_id = self.product_uom_qty
            elif 'product_id' not in vals and 'product_uom_qty' in vals:
                pr_id = self.product_id.id
                qty_id = vals['product_uom_qty']
            if picking_type.code in ('internal','outgoing'):
                if pr_id != False:
                    sum_quant_picking_draft_all = 0.0
                    sum_quant_so_draft_all = 0.0
                    prod_name = self.env['product.product'].browse(pr_id)
                    requesting_quant = qty_id
                    loc_ids = []
                    if picking_location:
                        loc_ids.append(picking_location.id)
                        for line in picking_location.child_ids:
                            loc_ids.append(line.id)
                    if len(loc_ids)==1:
                        quants_data = self.env['stock.quant'].search([('product_id','=',pr_id),('location_id','=',loc_ids[0])])
                    else:
                        quants_data = self.env['stock.quant'].search([('product_id','=',pr_id),('location_id','=',tuple(loc_ids))])
                    avail_quant_sum = 0.0
                    quantity = 0.0
                    reserved_quantity = 0.0
                    avail_quant = 0.0
                    if quants_data:
                        for quant_line in quants_data:
                            quantity += quant_line.quantity
                            reserved_quantity += quant_line.reserved_quantity
                        avail_quant = quantity - reserved_quantity
                        avail_quant_sum = avail_quant_sum + avail_quant
                    picking_types = self.env['stock.picking.type'].search([('code','in',('internal','outgoing')),('default_location_src_id','=',picking_location.id)])
                    list_type = []
                    for line0 in picking_types:
                        list_type.append(line0.id)
                    draft_picking_data = self.env['stock.picking'].search([('picking_type_id','in',list_type),('state','=','draft'),('location_id','=',picking_location.id)])
                    sum_quant_picking_draft = 0.0
                    if draft_picking_data:
                        for line2 in draft_picking_data:
                            onebyone_quant_picking_draft = 0.0
                            if line2.move_line_ids_without_package:
                                for line3 in line2.move_line_ids_without_package:
                                    if line3.product_id.id == pr_id:
                                        onebyone_quant_picking_draft = line3.qty_done
                            elif line2.move_ids_without_package:
                                for line32 in line2.move_ids_without_package:
                                    if line32.product_id.id == pr_id:
                                        onebyone_quant_picking_draft = line32.product_uom_qty
                            elif line2.move_line_nosuggest_ids:
                                for line33 in line2.move_line_nosuggest_ids:
                                    if line33.product_id.id == pr_id:
                                        onebyone_quant_picking_draft = line33.qty_done
                            sum_quant_picking_draft = sum_quant_picking_draft + onebyone_quant_picking_draft
                    sum_quant_picking_draft_all = sum_quant_picking_draft_all + sum_quant_picking_draft
                    if self.product_uom_qty > 0:
                        bolomjit_too = avail_quant_sum - sum_quant_picking_draft_all + self.product_uom_qty
                    else:
                        bolomjit_too = avail_quant_sum - sum_quant_picking_draft_all
                    if bolomjit_too < requesting_quant:
                        raise ValidationError(_('''Do not order in large quantities from the POSSIBLE BALANCE at the location! 
                        Product name : %s, 
                        On hand Quantity : + %s,
                        Reserved Quantity : - %s,
                        Draft Pickings : - %s,
                        Available Quantity: = %s, 
                        Ordering Quantity : %s. 
                        You cannot order more than %s! Check the balance carefully!''')%(prod_name.name,quantity,reserved_quantity,sum_quant_picking_draft_all,bolomjit_too,qty_id,bolomjit_too))
                    else:
                        sum_quant_picking_draft_all = 0.0
                        sum_quant_so_draft_all = 0.0        #END
                        
            if self.env.context.get('do_not_unreserve') is None:
                move_to_unreserve = self.filtered(
                    lambda m: m.state not in ['draft', 'done', 'cancel'] and float_compare(m.reserved_availability, vals.get('product_uom_qty'), precision_rounding=m.product_uom.rounding) == 1
                )
                move_to_unreserve._do_unreserve()
                (self - move_to_unreserve).filtered(lambda m: m.state == 'assigned').write({'state': 'partially_available'})
                receipt_moves_to_reassign |= move_to_unreserve.filtered(lambda m: m.location_id.usage == 'supplier')
                receipt_moves_to_reassign |= (self - move_to_unreserve).filtered(lambda m: m.location_id.usage == 'supplier' and m.state in ('partially_available', 'assigned'))
        propagated_date_field = False
        if vals.get('date_expected'):
            propagated_date_field = 'date_expected'
        elif vals.get('state', '') == 'done' and vals.get('date'):
            propagated_date_field = 'date'
        if propagated_date_field:
            new_date = fields.Datetime.to_datetime(vals.get(propagated_date_field))
            for move in self:
                move_dest_ids = move.move_dest_ids.filtered(lambda m: m.state not in ('done', 'cancel'))
                if not move_dest_ids:
                    continue
                delta_days = (new_date - move.date_expected).total_seconds() / 86400
                if not move.propagate_date or abs(delta_days) < move.propagate_date_minimum_delta:
                    move_dest_ids._delay_alert_log_activity('manual', move)
                    continue
                for move_dest in move_dest_ids:
                    new_move_date = max(move_dest.date_expected + relativedelta.relativedelta(days=delta_days or 0), fields.Datetime.now())
                    move_dest.date_expected = new_move_date
                move_dest_ids._delay_alert_log_activity('auto', move)
        track_pickings = (
            not self._context.get('mail_notrack')
            and not self._context.get('tracking_disable')
            and any(field in vals for field in ['state', 'picking_id'])
        )
        if track_pickings:
            to_track_picking_ids = {move.picking_id.id for move in self if move.picking_id}
            if vals.get('picking_id'):
                to_track_picking_ids.add(vals['picking_id'])
            to_track_picking_ids = list(to_track_picking_ids)
            pickings = self.env['stock.picking'].browse(to_track_picking_ids)
            initial_values = dict((picking.id, {'state': picking.state}) for picking in pickings)

        res = super(StockMove, self).write(vals)
        if track_pickings:
            pickings.message_track(pickings.fields_get(['state']), initial_values)
        if receipt_moves_to_reassign:
            receipt_moves_to_reassign._action_assign()
        return res

class StockMoveLine(models.Model):
    _inherit = "stock.move.line"

    def write(self, vals):
        if self.env.context.get('bypass_reservation_update'):
            return super(StockMoveLine, self).write(vals)
        defaults = self.default_get(['name', 'picking_type_id'])
        picking_type = self.picking_id.picking_type_id        #START
        picking_location = self.picking_id.location_id
        pr_id = False
        qty_id = 0.0
        if 'product_id' in vals and 'qty_done' in vals:
            pr_id = vals['product_id']
            qty_id = vals['qty_done']
        elif 'product_id' in vals and 'qty_done' not in vals:
            pr_id = vals['product_id']
            qty_id = self.qty_done
        elif 'product_id' not in vals and 'qty_done' in vals:
            pr_id = self.product_id.id
            qty_id = vals['qty_done']
        if picking_type.code in ('internal','outgoing'):
            if pr_id != False:
                sum_quant_picking_draft_all = 0.0
                sum_quant_so_draft_all = 0.0
                prod_name = self.env['product.product'].browse(pr_id)
                requesting_quant = qty_id
                loc_ids = []
                if picking_location:
                    loc_ids.append(picking_location.id)
                    for line in picking_location.child_ids:
                        loc_ids.append(line.id)
                if len(loc_ids)==1:
                    quants_data = self.env['stock.quant'].search([('product_id','=',pr_id),('location_id','=',loc_ids[0])])
                else:
                    quants_data = self.env['stock.quant'].search([('product_id','=',pr_id),('location_id','=',tuple(loc_ids))])
                avail_quant_sum = 0.0
                quantity = 0.0
                reserved_quantity = 0.0
                avail_quant = 0.0
                if quants_data:
                    for quant_line in quants_data:
                        quantity += quant_line.quantity
                        reserved_quantity += quant_line.reserved_quantity
                    avail_quant = quantity - reserved_quantity
                    avail_quant_sum = avail_quant_sum + avail_quant
                picking_types = self.env['stock.picking.type'].search([('code','in',('internal','outgoing')),('default_location_src_id','=',picking_location.id)])
                list_type = []
                for line0 in picking_types:
                    list_type.append(line0.id)
                draft_picking_data = self.env['stock.picking'].search([('picking_type_id','in',list_type),('state','=','draft'),('location_id','=',picking_location.id)])
                sum_quant_picking_draft = 0.0
                if draft_picking_data:
                    for line2 in draft_picking_data:
                        onebyone_quant_picking_draft = 0.0
                        if line2.move_line_ids_without_package:
                            for line3 in line2.move_line_ids_without_package:
                                if line3.product_id.id == pr_id:
                                    onebyone_quant_picking_draft = line3.qty_done
                        elif line2.move_ids_without_package:
                            for line32 in line2.move_ids_without_package:
                                if line32.product_id.id == pr_id:
                                    onebyone_quant_picking_draft = line32.product_uom_qty
                        elif line2.move_line_nosuggest_ids:
                            for line33 in line2.move_line_nosuggest_ids:
                                if line33.product_id.id == pr_id:
                                    onebyone_quant_picking_draft = line33.qty_done
                        sum_quant_picking_draft = sum_quant_picking_draft + onebyone_quant_picking_draft
                sum_quant_picking_draft_all = sum_quant_picking_draft_all + sum_quant_picking_draft
                if self.product_uom_qty > 0:
                    bolomjit_too = avail_quant_sum - sum_quant_picking_draft_all + self.product_uom_qty
                else:
                    bolomjit_too = avail_quant_sum - sum_quant_picking_draft_all
                if bolomjit_too < requesting_quant:
                    raise ValidationError(_('''Do not order in large quantities from the POSSIBLE BALANCE at the location! 
                        Product name : %s, 
                        On hand Quantity : + %s,
                        Reserved Quantity : - %s,
                        Draft Pickings : - %s,
                        Available Quantity: = %s, 
                        Ordering Quantity : %s. 
                        You cannot order more than %s! Check the balance carefully!''')%(prod_name.name,quantity,reserved_quantity,sum_quant_picking_draft_all,bolomjit_too,qty_id,bolomjit_too))
                else:
                    sum_quant_picking_draft_all = 0.0
                    sum_quant_so_draft_all = 0.0            #END

        if 'product_id' in vals and any(vals.get('state', ml.state) != 'draft' and vals['product_id'] != ml.product_id.id for ml in self):
            raise UserError(_("Changing the product is only allowed in 'Draft' state."))
        moves_to_recompute_state = self.env['stock.move']
        Quant = self.env['stock.quant']
        precision = self.env['decimal.precision'].precision_get('Product Unit of Measure')
        triggers = [
            ('location_id', 'stock.location'),
            ('location_dest_id', 'stock.location'),
            ('lot_id', 'stock.production.lot'),
            ('package_id', 'stock.quant.package'),
            ('result_package_id', 'stock.quant.package'),
            ('owner_id', 'res.partner')
        ]
        updates = {}
        for key, model in triggers:
            if key in vals:
                updates[key] = self.env[model].browse(vals[key])
        if updates or 'product_uom_qty' in vals:
            for ml in self.filtered(lambda ml: ml.state in ['partially_available', 'assigned'] and ml.product_id.type == 'product'):
                if 'product_uom_qty' in vals:
                    new_product_uom_qty = ml.product_uom_id._compute_quantity(
                        vals['product_uom_qty'], ml.product_id.uom_id, rounding_method='HALF-UP')
                    if float_compare(new_product_uom_qty, 0, precision_rounding=ml.product_id.uom_id.rounding) < 0:
                        raise UserError(_('Reserving a negative quantity is not allowed.'))
                else:
                    new_product_uom_qty = ml.product_qty
                if not ml._should_bypass_reservation(ml.location_id):
                    try:
                        Quant._update_reserved_quantity(ml.product_id, ml.location_id, -ml.product_qty, lot_id=ml.lot_id, package_id=ml.package_id, owner_id=ml.owner_id, strict=True)
                    except UserError:
                        if ml.lot_id:
                            Quant._update_reserved_quantity(ml.product_id, ml.location_id, -ml.product_qty, lot_id=False, package_id=ml.package_id, owner_id=ml.owner_id, strict=True)
                        else:
                            raise
                if not ml._should_bypass_reservation(updates.get('location_id', ml.location_id)):
                    reserved_qty = 0
                    try:
                        q = Quant._update_reserved_quantity(ml.product_id, updates.get('location_id', ml.location_id), new_product_uom_qty, lot_id=updates.get('lot_id', ml.lot_id),
                                                             package_id=updates.get('package_id', ml.package_id), owner_id=updates.get('owner_id', ml.owner_id), strict=True)
                        reserved_qty = sum([x[1] for x in q])
                    except UserError:
                        if updates.get('lot_id'):
                            # If we were not able to reserve on tracked quants, we can use untracked ones.
                            try:
                                q = Quant._update_reserved_quantity(ml.product_id, updates.get('location_id', ml.location_id), new_product_uom_qty, lot_id=False,
                                                                     package_id=updates.get('package_id', ml.package_id), owner_id=updates.get('owner_id', ml.owner_id), strict=True)
                                reserved_qty = sum([x[1] for x in q])
                            except UserError:
                                pass
                    if reserved_qty != new_product_uom_qty:
                        new_product_uom_qty = ml.product_id.uom_id._compute_quantity(reserved_qty, ml.product_uom_id, rounding_method='HALF-UP')
                        moves_to_recompute_state |= ml.move_id
                        ml.with_context(bypass_reservation_update=True).product_uom_qty = new_product_uom_qty

        next_moves = self.env['stock.move']
        if updates or 'qty_done' in vals:
            mls = self.filtered(lambda ml: ml.move_id.state == 'done' and ml.product_id.type == 'product')
            if not updates:  # we can skip those where qty_done is already good up to UoM rounding
                mls = mls.filtered(lambda ml: not float_is_zero(ml.qty_done - vals['qty_done'], precision_rounding=ml.product_uom_id.rounding))
            for ml in mls:
                qty_done_orig = ml.move_id.product_uom._compute_quantity(ml.qty_done, ml.move_id.product_id.uom_id, rounding_method='HALF-UP')
                in_date = Quant._update_available_quantity(ml.product_id, ml.location_dest_id, -qty_done_orig, lot_id=ml.lot_id,
                                                      package_id=ml.result_package_id, owner_id=ml.owner_id)[1]
                Quant._update_available_quantity(ml.product_id, ml.location_id, qty_done_orig, lot_id=ml.lot_id,
                                                      package_id=ml.package_id, owner_id=ml.owner_id, in_date=in_date)

                product_id = ml.product_id
                location_id = updates.get('location_id', ml.location_id)
                location_dest_id = updates.get('location_dest_id', ml.location_dest_id)
                qty_done = vals.get('qty_done', ml.qty_done)
                lot_id = updates.get('lot_id', ml.lot_id)
                package_id = updates.get('package_id', ml.package_id)
                result_package_id = updates.get('result_package_id', ml.result_package_id)
                owner_id = updates.get('owner_id', ml.owner_id)
                quantity = ml.move_id.product_uom._compute_quantity(qty_done, ml.move_id.product_id.uom_id, rounding_method='HALF-UP')
                if not ml._should_bypass_reservation(location_id):
                    ml._free_reservation(product_id, location_id, quantity, lot_id=lot_id, package_id=package_id, owner_id=owner_id)
                if not float_is_zero(quantity, precision_digits=precision):
                    available_qty, in_date = Quant._update_available_quantity(product_id, location_id, -quantity, lot_id=lot_id, package_id=package_id, owner_id=owner_id)
                    if available_qty < 0 and lot_id:
                        untracked_qty = Quant._get_available_quantity(product_id, location_id, lot_id=False, package_id=package_id, owner_id=owner_id, strict=True)
                        if untracked_qty:
                            taken_from_untracked_qty = min(untracked_qty, abs(available_qty))
                            Quant._update_available_quantity(product_id, location_id, -taken_from_untracked_qty, lot_id=False, package_id=package_id, owner_id=owner_id)
                            Quant._update_available_quantity(product_id, location_id, taken_from_untracked_qty, lot_id=lot_id, package_id=package_id, owner_id=owner_id)
                            if not ml._should_bypass_reservation(location_id):
                                ml._free_reservation(ml.product_id, location_id, untracked_qty, lot_id=False, package_id=package_id, owner_id=owner_id)
                    Quant._update_available_quantity(product_id, location_dest_id, quantity, lot_id=lot_id, package_id=result_package_id, owner_id=owner_id, in_date=in_date)
                next_moves |= ml.move_id.move_dest_ids.filtered(lambda move: move.state not in ('done', 'cancel'))

                if ml.picking_id:
                    ml._log_message(ml.picking_id, ml, 'stock.track_move_template', vals)

        res = super(StockMoveLine, self).write(vals)

        if 'qty_done' in vals:
            for move in self.mapped('move_id'):
                if move.scrapped:
                    move.scrap_ids.write({'scrap_qty': move.quantity_done})
        if updates or 'qty_done' in vals:
            moves = self.filtered(lambda ml: ml.move_id.state == 'done').mapped('move_id')
            moves |= self.filtered(lambda ml: ml.move_id.state not in ('done', 'cancel') and ml.move_id.picking_id.immediate_transfer and not ml.product_uom_qty).mapped('move_id')
            for move in moves:
                move.product_uom_qty = move.quantity_done
        next_moves._do_unreserve()
        next_moves._action_assign()
        if moves_to_recompute_state:
            moves_to_recompute_state._recompute_state()
        return res
