# -*- coding: utf-8 -*-
{
    "name": "Stock Picking check quantity",
    'summary': "Product QUANTS. Check qty. Check quantity. Check quants. Product qty. Product quantity. Available qty. Available product qty. Available quantity. Available product quantity.",
    "version": "13.0",
    "description": """
        This module allows you to check product quants with create pickings. Contact and support email: torbatj79@gmail.com
    """,
    "author": "Tb25",
    'support': 'torbatj79@gmail.com',
    "category": "Inventory",
    'price': 150.0,
    'currency': 'USD',
    "website": "",
    "license": "AGPL-3",
    'depends': ['stock'],
    'images': ['static/src/img/banner.png'],
    "auto_install": False,
    "installable": True,
    "application": True,
}